# -*- coding: utf-8 -*-
import os, sys, cv2, pytesseract
from PIL import Image
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from screeninfo import get_monitors
from Person import Person

class QSlideSwitchPrivate(QObject):
    
    def __init__(self, q):
        QObject.__init__(self)

        self._position = 0
        self._sliderShape = QRectF()
        self._gradient = QLinearGradient()
        self._gradient.setSpread(QGradient.PadSpread)
        self._qPointer = q

        self.animation = QPropertyAnimation(self)
        self.animation.setTargetObject(self)
        self.animation.setPropertyName(b"position")  
        self.animation.setStartValue(0)
        self.animation.setEndValue(1)
        self.animation.setDuration(300)
        self.animation.setEasingCurve(QEasingCurve.InOutExpo)

    def __del__(self):
        del self.animation

    @pyqtProperty(float)
    def position(self):
        return self._position

    @position.setter
    def position(self, value):
        self._position = value
        self._qPointer.repaint()

    def drawSlider(self, painter):
        margin = 3
        r = self._qPointer.rect().adjusted(0,0,-1,-1)
        dx = (r.width() - self._sliderShape.width()) * self._position
        sliderRect = self._sliderShape.translated(dx, 0)
        painter.setPen(Qt.NoPen)

        # basic settings
        shadow = self._qPointer.palette().color(QPalette.Dark)
        light = self._qPointer.palette().color(QPalette.Light)
        button = self._qPointer.palette().color(QPalette.Button)

        # draw background
        # draw outer background
        self._gradient.setColorAt(0, shadow.darker(130))
        self._gradient.setColorAt(1, light.darker(130))
        self._gradient.setStart(0, r.height())
        self._gradient.setFinalStop(0, 0)
        painter.setBrush(self._gradient)
        painter.drawRoundedRect(r, 15, 15)

        # draw background
        # draw inner background
        self._gradient.setColorAt(0, shadow.darker(140))
        self._gradient.setColorAt(1, light.darker(160))
        self._gradient.setStart(0, 0)
        self._gradient.setFinalStop(0, r.height())
        painter.setBrush(self._gradient)
        painter.drawRoundedRect(r.adjusted(margin, margin, -margin, -margin), 15, 15)

        # draw slider
        self._gradient.setColorAt(0, button.darker(130))
        self._gradient.setColorAt(1, button)

        # draw outer slider
        self._gradient.setStart(0, r.height())
        self._gradient.setFinalStop(0, 0)
        painter.setBrush(self._gradient)
        painter.drawRoundedRect(sliderRect.adjusted(margin, margin, -margin, -margin), 10, 15)

        # draw inner slider
        self._gradient.setStart(0, 0)
        self._gradient.setFinalStop(0, r.height())
        painter.setBrush(self._gradient)
        painter.drawRoundedRect(sliderRect.adjusted(2.5 * margin, 2.5 * margin, -2.5 * margin, - 2.5 * margin), 5, 15)

        # draw text
        if self.animation.state() == QPropertyAnimation.Running:
            return #don't draw any text while animation is running

        font = self._qPointer.font()
        self._gradient.setColorAt(0, light)
        self._gradient.setColorAt(1, shadow)
        self._gradient.setStart(0, r.height() / 2.0 + font.pointSizeF())
        self._gradient.setFinalStop(0, r.height() / 2.0 - font.pointSizeF())
        painter.setFont(font)
        painter.setPen(QPen(QBrush(self._gradient), 0))

        if self._qPointer.isChecked():
            painter.drawText(0, 0, r.width() / 2, r.height()-1, Qt.AlignCenter, "ON")
        else:
            painter.drawText( r.width() / 2, 0, r.width() / 2, r.height() - 1, Qt.AlignCenter, "OFF")

    def updateSliderRect(self, size):
        self._sliderShape.setWidth(size.width() / 2.0)
        self._sliderShape.setHeight(size.height() - 1.0)

    @pyqtSlot(bool, name='animate')
    def animate(self, checked):
        self.animation.setDirection(QPropertyAnimation.Forward if checked else QPropertyAnimation.Backward)
        self.animation.start()


class QSlideSwitch(QAbstractButton):
    def __init__(self, parent = None):
        super(QAbstractButton, self).__init__(parent)

        self.d_ptr = QSlideSwitchPrivate( self )
        self.clicked.connect( self.d_ptr.animate )
        self.d_ptr.animation.finished.connect( self.update )

    def __del__(self):
        del self.d_ptr

    def sizeHint(self):
        return QSize(48, 28)

    def hitButton(self, point):
        return self.rect().contains(point)

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing)
        self.d_ptr.drawSlider(painter)

    def resizeEvent(self, event):
        self.d_ptr.updateSliderRect(event.size())
        self.repaint()


class Ui_MainWindow(object):

    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName("Recognition")
        MainWindow.resize(1200, 1000)

        for m in get_monitors():
            monitor = m

        MainWindow.setFixedSize(1200, 1000)
        MainWindow.move((monitor.width - 1200)/2, (monitor.height - 1000)/2)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")

        self.widget = QWidget(self.centralwidget)
        self.widget.setObjectName("widget")
        self.widget.setGeometry(QRect(0, 0, 1200, 1000))

        self.gridLayoutWidget = QWidget(self.widget)
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(0, 0, 1200, 1000))
        self.gridLayout = QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setObjectName("gridLayout")
        self.gridLayout.setContentsMargins(5, 5, 5, 5)

        self.label = QLabel()
        self.label.setObjectName(u"label")
        self.pixmap = QPixmap("test.jpg")
        self.pixmap = self.pixmap.scaled(500,500, Qt.KeepAspectRatio, Qt.SmoothTransformation)
        self.label.resize(500,500)
        self.label.setPixmap(self.pixmap)
        #self.label.setScaledContents(True)
        self.label.setAlignment(Qt.AlignLeft)
        
        self.gridLayout.addWidget(self.label,0,0)

        font = QFont()
        font.setPointSize(24)
        self.label2 = QLabel()
        self.label2.setFont(font)
        #self.gridLayout.addWidget(self.label2,0,1)

        self.tableWidget = QTableWidget()
        self.tableWidget.setRowCount(5)
        self.tableWidget.setColumnCount(3)
        self.tableWidget.setObjectName("tableWidget")
        #self.tableWidget.setGeometry(QRect(0, 510, 1071, 351))
        self.gridLayout.addWidget(self.tableWidget, 1, 0, 2, 2)

        self.item = QTableWidgetItem("Hello world")
        self.tableWidget.setItem(0,0,self.item)

        self.horizontal = QWidget()
        self.hbox = QHBoxLayout()
        self.hbox.addStretch(1)
        self.horizontal.setLayout(self.hbox)
        switcher = QSlideSwitch()
        switcher.setCheckable(True)
        switcher.setGeometry(0, 0, 50, 50)
        self.hbox.addWidget(switcher)
        self.gridLayout.addWidget(self.horizontal, 2, 0, 2, 2)

        self.show_person(Person())

    def updateLabel(self, text):
        self.label2.setText(text)

    def show_person(self, person):
        vbox = QVBoxLayout()
        #vbox.addStretch(1)
        name = QLabel()
        name.setText(person.name)
        surname = QLabel()
        surname.setText(person.surname)
        age = QLabel()
        age.setText(str(person.age))
        middle_name = QLabel()
        middle_name.setText(person.middle_name)
        car_model = QLabel()
        car_model.setText(person.car_model)
        state_number = QLabel()
        state_number.setText(person.state_number)

        vbox.addWidget(name)
        vbox.addWidget(surname)
        vbox.addWidget(middle_name)
        vbox.addWidget(age)
        vbox.addWidget(car_model)
        vbox.addWidget(state_number)

        widget = QWidget()
        widget.setLayout(vbox)
        self.gridLayout.addWidget(widget, 0, 1)

class Recognizer:
    def __init__(self):
        pass

    def recognize(self):
        image = 'test.jpg'
        preprocess = "thresh"
        image = cv2.imread(image)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        if preprocess == "thresh":
            gray = cv2.threshold(gray, 0, 255,
                cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
        elif preprocess == "blur":
            gray = cv2.medianBlur(gray, 3)

        filename = "1.png".format(os.getpid())
        cv2.imwrite(filename, gray)

        custom_config = r'--oem 3 --psm 6'
        text = pytesseract.image_to_string(Image.open("2.png"), config = custom_config)
        print(text)
        return text

if __name__ == "__main__":
    app = QApplication(sys.argv)

    Form = QWidget()
    ui = Ui_MainWindow()
    ui.setupUi(Form)

    rec = Recognizer()
    text = rec.recognize()
    #ui.updateLabel(text)

    Form.repaint()
    Form.show()
    sys.exit(app.exec_())



