import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls.Material 2.0
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.11
import Qt.labs.qmlmodels 1.0

Window {
    id: window
    width: 1200
    height: 1000
    visible: true
    title: "Система управления"

    Grid {
        id: grid
        anchors.fill: parent
        anchors.topMargin: 0
        columnSpacing: 0
        rowSpacing: 0
        rows: 3
        columns: 2
        Image {
            id: image
            width: 700
            height: 500
            horizontalAlignment: Image.AlignLeft
            verticalAlignment: Image.AlignTop
            source: "../test.jpg"
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            Layout.maximumHeight: 500
            Layout.maximumWidth: 700
            clip: false
            mipmap: true
            autoTransform: true
            asynchronous: true
            cache: true
            Layout.fillHeight: true
            Layout.fillWidth: true
            fillMode: Image.PreserveAspectFit
        }

        Row {
            id: row
            x: 0
            width: 500
            height: 500
            Layout.columnSpan: 1
            Layout.rowSpan: 1
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            Layout.fillHeight: true
            Layout.fillWidth: true

            Label {
                id: label
                text: qsTr("Label")
            }

            Label {
                id: label1
                text: qsTr("Label")
            }

            Label {
                id: label2
                text: qsTr("Label")
            }
        }

        TableView {
            anchors.fill: parent
            interactive: false
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.columnSpan: 2
            Layout.rowSpan: 2
            columnSpacing: 0
            rowSpacing: 0
            clip: false

            model: TableModel {
                TableModelColumn { display: "name" }
                TableModelColumn { display: "color" }

                rows: [
                    {
                        "name": "cat",
                        "color": "black"
                    },
                    {
                        "name": "dog",
                        "color": "brown"
                    },
                    {
                        "name": "bird",
                        "color": "white"
                    }
                ]
            }

            delegate: Rectangle {
                implicitWidth: 100
                implicitHeight: 50
                border.width: 1

                Text {
                    text: display
                    anchors.centerIn: parent
                }
            }
        }

        Column {
            id: column
            x: 0
            width: 200
            height: 400
            visible: true
            Layout.fillHeight: true
            Layout.fillWidth: true

            Button {
                id: button
                text: qsTr("Button")
            }
        }


    }
}
